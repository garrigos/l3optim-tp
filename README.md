# Travaux Pratiques pour le cours d'Optimisation

Cetta page regroupe tous les documents nécéssaires pour réaliser les TPs du [cours d'Optimisation](https://moodle.u-paris.fr/course/view.php?id=2434) (Université de Paris - L3 MFA/MI). 

## Instructions pour lancer un TP depuis les salles info

Lors de votre **premier TP**:

1. Télécharger le dossier `.zip` à [cette adresse](https://gitlab.math.univ-paris-diderot.fr/garrigos/l3optim-tp/-/archive/master/l3optim-tp-master.zip). Si vous êtes sous Ubuntu votre téléchargement se trouve certainement dans votre dossier *Téléchargment*.
2. Extraire l'archive, et placer le dossier décompressé à l'emplacement de votre choix. Je vous suggère de le mettre quelque part dans *Documents*, afin de pouvoir le retrouver aisément par la suite. Vous pouvez également le renommer si vous le souhaitez.

Pour lancer un TP:

1. Ouvrir un terminal dans le dossier contenant les fichiers du TP. Vous pouvez le faire en faisant clic-droit dans l'explorateur de fichier > ouvrir un terminal dans ce dossier.
2. Dans le terminal, lancer la commande `jupyter notebook`. Cela va ouvrir une page sur votre navigateur.
3. Cliquer sur le TP du jour.

## Instructions pour lancer un TP sur un serveur mybinder.org

Pour lancer le TP, ouvrez le lien du TP correspondant dans un nouvel onglet :

- [TP 1](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.math.univ-paris-diderot.fr%2Fgarrigos%2Fl3optim-tp/HEAD?filepath=TP1_intro_newton.ipynb) et [sa correction](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.math.univ-paris-diderot.fr%2Fgarrigos%2Fl3optim-tp/HEAD?filepath=TP1_intro_newton_correction.ipynb)
- [TP 2](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.math.univ-paris-diderot.fr%2Fgarrigos%2Fl3optim-tp/HEAD?filepath=TP2_regression_lineaire.ipynb) et [sa correction](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.math.univ-paris-diderot.fr%2Fgarrigos%2Fl3optim-tp/HEAD?filepath=TP2_regression_lineaire_correction.ipynb)
- [TP 3](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.math.univ-paris-diderot.fr%2Fgarrigos%2Fl3optim-tp/HEAD?filepath=TP3_fonctions_multivariees.ipynb) et [sa correction](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.math.univ-paris-diderot.fr%2Fgarrigos%2Fl3optim-tp/HEAD?filepath=TP3_fonctions_multivariees_correction.ipynb)
- [TP 4](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.math.univ-paris-diderot.fr%2Fgarrigos%2Fl3optim-tp/HEAD?filepath=TP4_defloutage_gradient.ipynb) et [sa correction](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.math.univ-paris-diderot.fr%2Fgarrigos%2Fl3optim-tp/HEAD?filepath=TP4_defloutage_gradient_correction.ipynb)
- [TP 5](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.math.univ-paris-diderot.fr%2Fgarrigos%2Fl3optim-tp/HEAD?filepath=TP5_classification_svm.ipynb)

Cela va lancer une instance de myBinder, qui vous permettra de coder en ligne, sans ne rien avoir à installer sur votre PC. 

:warning: Tenez compte du fait que:

- Le temps que le serveur se lance peut prendre du **temps** (jusqu'à 2 minutes). Soyez donc patients. 
- Une fois le notebook ouvert, vous devez exécuter avant tout la commande suivante :
```
import mybinder
mybinder.start_session()
```
Sinon le serveur s'arrêtera après 10 minutes. Cela veut dire que vous **perdrez vos données** et devrez tout recommencer de zéro  :pensive:
- Tout ce que vous ferez ne sera **pas** sauvegardé en ligne. Une fois votre TP terminé, si vous souhaitez sauvegarder votre travail, il vous faudra télécharger votre notebook en cliquant en dans la barre du haut sur le bouton `Download`. (Pour des raisons qui m'échappent encore, le fichier téléchargé peut avoir 2 extensions différentes : `.json` ou `.ipynb`. Si vous avez un `.json` il faut remplacer l'extension par `.ipynb`)

|  |
| ---  |
|<img src="images/binder_load.png" width=400px>  |
| La page de chargement du TP. Peut prendre 1-2 minutes. |



|  |
| ---  |
|<img src="images/binder_TP.png" width=400px>  |
| L'interface Jupyter contenant le TP. |




|  |
| ---  |
|<img src="images/binder_download.png" width=100px>  |
| Le bouton de téléchargement du TP. |



